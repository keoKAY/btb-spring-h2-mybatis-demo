create table person_tb(

                          id integer not null,
                          username varchar(255) not null,
                          gender varchar (10) not null,
                          bio varchar(255) not null,
                          primary key (id)
);

insert into person_tb(id,username,gender,bio) values (1,'Data1','Male','Here is my bio');
insert into person_tb(id,username,gender,bio) values (2,'Data2','Male','Here is my bio');
insert into person_tb(id,username,gender,bio) values (3,'Data3','Male','Here is my bio');
insert into person_tb(id,username,gender,bio) values (4,'Data4','Male','Here is my bio');