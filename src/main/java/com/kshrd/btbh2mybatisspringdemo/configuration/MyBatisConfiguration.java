package com.kshrd.btbh2mybatisspringdemo.configuration;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.kshrd.btbh2mybatisspringdemo.repository")
public class MyBatisConfiguration {

    public DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    // put datasource in TransactionManager

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(){
        return new  DataSourceTransactionManager(dataSource);
    }

    // put datasource sqlSessionFactoryBean

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(){
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);

        return  sqlSessionFactoryBean;
    }
}
