package com.kshrd.btbh2mybatisspringdemo.repository;


import com.kshrd.btbh2mybatisspringdemo.model.Person;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository {


    @Select("select * from person_tb")
    List<Person> getAllPerson ();
}
