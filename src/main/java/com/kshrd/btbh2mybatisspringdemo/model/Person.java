package com.kshrd.btbh2mybatisspringdemo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Person {
    private int id;
    private String username;
    private  String gender;
    private  String bio;


}
