package com.kshrd.btbh2mybatisspringdemo.controller;


import com.kshrd.btbh2mybatisspringdemo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    PersonService personService;

    @GetMapping
    public String index(){

        System.out.println("Here is the value of the person");
        personService.getAllPerson().stream().forEach(System.out::println);

        return "index";
    }


}
