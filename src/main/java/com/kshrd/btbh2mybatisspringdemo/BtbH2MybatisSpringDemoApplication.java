package com.kshrd.btbh2mybatisspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtbH2MybatisSpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtbH2MybatisSpringDemoApplication.class, args);
    }

}
