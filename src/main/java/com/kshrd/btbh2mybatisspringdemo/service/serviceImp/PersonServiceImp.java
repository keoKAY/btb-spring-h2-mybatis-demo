package com.kshrd.btbh2mybatisspringdemo.service.serviceImp;

import com.kshrd.btbh2mybatisspringdemo.model.Person;
import com.kshrd.btbh2mybatisspringdemo.repository.PersonRepository;
import com.kshrd.btbh2mybatisspringdemo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImp implements PersonService {

    // inject

    @Autowired
    PersonRepository personRepository;
    @Override
    public List<Person> getAllPerson() {
        return personRepository.getAllPerson();
    }
}
