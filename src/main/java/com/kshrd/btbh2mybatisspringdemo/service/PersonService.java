package com.kshrd.btbh2mybatisspringdemo.service;

import com.kshrd.btbh2mybatisspringdemo.model.Person;

import java.util.List;

public interface PersonService {

    List<Person> getAllPerson();
}
